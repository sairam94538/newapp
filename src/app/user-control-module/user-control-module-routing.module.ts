import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponentComponent } from './login-component/login-component.component';
import { NewPostComponentItem } from './new-post/new-post.component';
import { PostHistoryComponent } from './post-history/post-history.component';
import { HelpComponent } from './help/help.component';



const routes: Routes = [
  {path:'login', component:LoginComponentComponent},
  {path:'post', component:NewPostComponentItem},
  {path: 'history',component:PostHistoryComponent},
  {path: 'help', component:HelpComponent},  

  {path:'**', redirectTo:'login'}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserControlRoutingModule { }
