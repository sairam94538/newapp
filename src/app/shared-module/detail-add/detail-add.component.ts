import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-detail-add',
  templateUrl: './detail-add.component.html',
  styleUrls: ['./detail-add.component.css']
})
export class DetailAddComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

@Input() public detailData;

  public jsondataForCar = [
    {
      "auto": {
        "cars": {
          "postNumber": {
            "p1": {
              Title:"Used Chevrolet for Sale",
              Make: "Chevrolet",
              Model: "Spark",
              year: "2018",
              ExtColor: "White",
              IntColor: "Black",
              seatType: "cloth",
              Engine: "4 Cyl, 1.4L",
              Drivetrain: "Front Wheel Drive",
              Trans: "6-Speed Automatic w/OD",
              FuelType: "Gasoline",
              Mileage: "50,220",
              MPG: "29/40",
              VIN: "1G1BE5SM7J7242549",
              zip: "63124",
              price: "12999",
              image1: "/assets/images/car1.jpg",
              image2: "/assets/images/car2.jpg",
              image3: "/assets/images/car3.jpg",
              image4: "/assets/images/car4.jpg",
              description: "Front wheel drive. Engine control, stop-start system. Coolant protection, engine. Brake, parking, manual, foot apply. Brake lining, high-performance, noise and dust performance",
              email: "Chevrolet@gmail.com",
              phone: "3104329088"
            },
            "p2": {
              Title: "Hyundai Sonata Sports for $15,399",
              Make: "Hyundai",
              Model: "Sonata Sport 2.4L",
              year: "2017",
              ExtColor: "Gray",
              IntColor: "Gray",
              seatType: "cloth",
              Engine: "4 Cyl, 1.6L",
              Drivetrain: "Front Wheel Drive",
              Trans: "6-Speed Automatic w/OD",
              FuelType: "Gasoline",
              Mileage: "36040",
              MPG: "25/35",
              VIN: "5NPE34AF0HH511374",
              zip: "63042",
              price: "15399",
              image1: "/assets/images/car2.jpg",
              image2: "https://media-cdn-delta.jazelc.com/media/5633213",
              image3: "https://media-cdn-delta.jazelc.com/media/5633221",
              image4: "https://media-cdn-delta.jazelc.com/media/5633223",
              description: "Wipers, front intermittent, variable. Door handles, body-color. Windshield, solar absorbing. Glass, solar absorbing. Wheels, 16\" (40.6 cm) aluminum. Wheel, spare, 16\" (40.6 cm) steel",
              email: "Hyundai@gmail.com",
              phone: "4014239893"
            }
          }
        }
      }
    }
  ]
}
