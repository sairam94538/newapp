import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public jsonresonse =[
    {
      href:"https://www.tesla.com/",
      src:"/assets/images/giphy.gif"
    },
    {
      href:"https://www.tacobell.com/",
      src:"/assets/images/drive-thru-pre.gif"
    },
    {
      href:"https://www.apple.com/",
      src:"/assets/images/1510212849847030107.gif"
    },
    {
      href:"https://www.dodge.com/challenger.html",
      src:"/assets/images/AggressiveUnfortunateBlackfly-size_restricted.gif"
    }
  ]

}
