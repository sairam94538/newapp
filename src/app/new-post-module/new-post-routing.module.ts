import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewpostformComponent } from './newpostform/newpostform.component';


const postroutes: Routes = [
  {path:'new', component:NewpostformComponent},
  {path:'**',redirectTo:'viewpost'}
];

@NgModule({
  imports: [RouterModule.forChild(postroutes)],
  exports: [RouterModule]
})
export class NewPostRoutingModule { }
