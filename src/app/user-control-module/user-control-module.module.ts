import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponentComponent } from './login-component/login-component.component';
import { NewPostComponentItem } from './new-post/new-post.component';
import { PostHistoryComponent } from './post-history/post-history.component';
import { HelpComponent } from './help/help.component';
import { UserControlRoutingModule } from './user-control-module-routing.module';



@NgModule({
  declarations: [LoginComponentComponent, NewPostComponentItem, PostHistoryComponent, HelpComponent],
  imports: [
    CommonModule,
    UserControlRoutingModule
  ]
})
export class UserControlModuleModule { }
