import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoryListComponentComponent } from './category-list-component/category-list-component.component';
import { AfterSearchComponent } from './after-search/after-search.component';
import { SearchDetailsComponent } from './search-details/search-details.component';
import { ViewPostComponent } from './view-post/view-post.component';


const homeroutes: Routes = [
  {path:'category', component:CategoryListComponentComponent},
  {path:'afterSearch', component:AfterSearchComponent},
  {path:'serachDetails', component:SearchDetailsComponent},
  {path:'viewpost', component:ViewPostComponent},
  {path:'**',redirectTo:'viewpost'}
];

@NgModule({
  imports: [RouterModule.forChild(homeroutes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
