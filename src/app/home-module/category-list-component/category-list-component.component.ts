import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-category-list-component',
  templateUrl: './category-list-component.component.html',
  styleUrls: ['./category-list-component.component.css']
})
export class CategoryListComponentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Output() itemClicked= new EventEmitter<string>();

  public itemSelected(itemClicked:any){
    console.log("Data sending to >>>>>>> view-post-component");
    console.log(itemClicked);
    this.itemClicked.emit(itemClicked);
  }

  // catogoryList[0].name ->Housing  ---------- catogoryList[0] - item
  // catogoryList[1].name ->Job
  // catogoryList[0].items ->[
  //   {name:"1BHK Rent", link:"/1bhk"},
  //   {name:"2BHK Rent", link:"/1bhk"},
  //   {name:"3BHK Rent", link:"/1bhk"},
  //   {name:"1BHK Buy", link:"/1bhk"},
  //   {name:"2BHK Buy", link:"/1bhk"}
  // ]
  // catogoryList[0].items[0].name = "1BHK Rent"  --- catogoryList[0].items[0] -- subitem - item.items
  // catogoryList[0].items[1].name = "2BHK Rent"

  public catogoryList=[
    {
      name:"Housing",
      items:[
        {name:"1BHK Rent", link:"/1bhk"},
        {name:"2BHK Rent", link:"/1bhk"},
        {name:"3BHK Rent", link:"/1bhk"},
        {name:"1BHK Buy", link:"/1bhk"},
        {name:"2BHK Buy", link:"/1bhk"}
      ]
    },
    {
      name:"Job",
      items:[
        {name:"Designer", link:"/1bhk"},
        {name:"Driver", link:"/1bhk"},
        {name:"IT Industry", link:"/1bhk"}
      ]
    },
    {
      name:"Sale",
      items:[
        {name:"House", link:"/1bhk"},
        {name:"Car", link:"/1bhk"}
      ]
    }
  ]

}
