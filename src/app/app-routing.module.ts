import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModuleModule } from './shared-module/shared-module.module';


const routes: Routes = [
  // {path:'home', loadChildren:'./home-module/home-module.module#HomeRoutingModule'},
  {path:'home',loadChildren: () => import('./home-module/home-module.module').then(m => m.HomeModuleModule)},
  {path:'controle',loadChildren: () => import('./user-control-module/user-control-module.module').then(m =>m.UserControlModuleModule)},
  {path:'post',loadChildren: () => import('./new-post-module/new-post-module.module').then(m =>m.NewPostModuleModule)},
  // {path:'controle', loadChildren:'./user-control-module/user-control-module.module#UserControlModuleModule'},
  {path:'**', redirectTo:'home'}

];

@NgModule({
  
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
