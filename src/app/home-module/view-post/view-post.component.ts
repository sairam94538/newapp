import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-post',
  templateUrl: './view-post.component.html',
  styleUrls: ['./view-post.component.css']
})
export class ViewPostComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public selectedScreen;
  public selectedCatogry;
  public detailData_fromAppListItem;

  public eventFrom_app_category_list_component(eventData:any){
    console.log("Data receiving from <<<<< eventFrom_app_category_list_component")
    console.log(eventData);
    this.selectedCatogry = eventData.name;
    this.selectedScreen = "itemlist";
  }

  public detailData_fromAppListItem_list_component(eventDataList:any){
    
    this.detailData_fromAppListItem = eventDataList;
    this.selectedScreen ='detaillist';
  }
}
