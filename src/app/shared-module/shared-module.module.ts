import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonsListComponent } from './buttons-list/buttons-list.component';
import { ListItemComponent } from './list-item/list-item.component';
import { AddComponent } from './add/add.component';
import { DetailAddComponent } from './detail-add/detail-add.component';
import { NavigationInfoComponent } from './navigation-info/navigation-info.component';
import { SortingComponent } from './sorting/sorting.component';
import { NewPostComponent } from './new-post/new-post.component';



@NgModule({
  declarations: [ButtonsListComponent, ListItemComponent, AddComponent, DetailAddComponent, NavigationInfoComponent, SortingComponent, NewPostComponent],
  imports: [
    CommonModule
  ],
  exports:[ButtonsListComponent, ListItemComponent,AddComponent,DetailAddComponent, NavigationInfoComponent,SortingComponent,NewPostComponent]
})
export class SharedModuleModule { }
