import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryListComponentComponent } from './category-list-component/category-list-component.component';
import { HomeRoutingModule } from './home-routing.module';
import { AfterSearchComponent } from './after-search/after-search.component';
import { SearchDetailsComponent } from './search-details/search-details.component';
import { ViewPostComponent } from './view-post/view-post.component';
import { SharedModuleModule } from '../shared-module/shared-module.module';



@NgModule({
  declarations: [CategoryListComponentComponent, AfterSearchComponent, SearchDetailsComponent, ViewPostComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModuleModule
  ]
})
export class HomeModuleModule { }
