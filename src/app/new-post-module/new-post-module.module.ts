import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewpostformComponent } from './newpostform/newpostform.component';
import { NewPostRoutingModule } from './new-post-routing.module';
import { SharedModuleModule } from '../shared-module/shared-module.module';



@NgModule({
  declarations: [NewpostformComponent],
  imports: [
    CommonModule,
    SharedModuleModule,
    NewPostRoutingModule
  ]
})
export class NewPostModuleModule { }
