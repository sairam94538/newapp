import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    
  }

  @Output() itemSelected:EventEmitter<any> = new EventEmitter<any>();
  public itemSelected1 = "sau";

  public moreInfo(selectedItem:any){
    console.log("Sending data on click of More Info >>>>")
    console.log(selectedItem);
    this.itemSelected.emit(selectedItem);    
  }

  @Input() selectedCatogory;

    public jsondata2:any =[
    {
     "postNumber":{
       "p1":{
        Make: "Chevrolet",
        Model: "Spark",
        year: "2018",
        ExtColor: "White",
        IntColor: "Black",
        seatType: "cloth",
        Engine: "4 Cyl, 1.4L",
        Drivetrain: "Front Wheel Drive",
        Trans: "6-Speed Automatic w/OD",
        FuelType: "Gasoline",
        Mileage: "50,220",
        MPG: "29/40",
        VIN: "1G1BE5SM7J7242549",
        zip: "63124",
        price: "12999",
        image1: "/assets/images/car1.jpg",
        image2: "/assets/images/car2.jpg",
        image3: "/assets/images/car3.jpg",
        image4: "/assets/images/car4.jpg",
        description: "Front wheel drive. Engine control, stop-start system. Coolant protection, engine. Brake, parking, manual, foot apply. Brake lining, high-performance, noise and dust performance",
        email: "Chevrolet@gmail.com",
        phone: "3104329088"
       },
       "p2":{
        Make: "Hyundai",
        Model: "Sonata Sport 2.4L",
        year: "2017",
        ExtColor: "Gray",
        IntColor: "Gray",
        seatType: "cloth",
        Engine: "4 Cyl, 1.6L",
        Drivetrain: "Front Wheel Drive",
        Trans: "6-Speed Automatic w/OD",
        FuelType: "Gasoline",
        Mileage: "36040",
        MPG: "25/35",
        VIN: "5NPE34AF0HH511374",
        zip: "63042",
        price: "15399",
        image1: "/assets/images/car2.jpg",
        image2: "/assets/images/car1.jpg",
        image3: "/assets/images/car1.jpg",
        image4: "/assets/images/car1.jpg",
        description: "Wipers, front intermittent, variable. Door handles, body-color. Windshield, solar absorbing. Glass, solar absorbing. Wheels, 16\" (40.6 cm) aluminum. Wheel, spare, 16\" (40.6 cm) steel",
        email: "Hyundai@gmail.com",
        phone: "4014239893"
       }
      }
    }
  ]
public jsondata = [
  {
    postNumber: "1",
    Make: "Chevrolet",
    Model: "Spark",
    year: "2018",
    ExtColor: "White",
    IntColor: "Black",
    seatType: "cloth",
    Engine: "4 Cyl, 1.4L",
    Drivetrain: "Front Wheel Drive",
    Trans: "6-Speed Automatic w/OD",
    FuelType: "Gasoline",
    Mileage: "50,220",
    MPG: "29/40",
    VIN: "1G1BE5SM7J7242549",
    zip: "63124",
    price: "12999",
    image1: "/assets/images/car1.jpg",
    image2: "/assets/images/car2.jpg",
    image3: "/assets/images/car3.jpg",
    image4: "/assets/images/car4.jpg",
    description: "Front wheel drive. Engine control, stop-start system. Coolant protection, engine. Brake, parking, manual, foot apply. Brake lining, high-performance, noise and dust performance",
    email: "Chevrolet@gmail.com",
    phone: "3104329088"
  },
  {
    postNumber: "2",
    Make: "Hyundai",
    Model: "Sonata Sport 2.4L",
    year: "2017",
    ExtColor: "Gray",
    IntColor: "Gray",
    seatType: "cloth",
    Engine: "4 Cyl, 1.6L",
    Drivetrain: "Front Wheel Drive",
    Trans: "6-Speed Automatic w/OD",
    FuelType: "Gasoline",
    Mileage: "36040",
    MPG: "25/35",
    VIN: "5NPE34AF0HH511374",
    zip: "63042",
    price: "15399",
    image1: "/assets/images/car2.jpg",
    image2: "https://media-cdn-delta.jazelc.com/media/5633213",
    image3: "https://media-cdn-delta.jazelc.com/media/5633221",
    image4: "https://media-cdn-delta.jazelc.com/media/5633223",
    description: "Wipers, front intermittent, variable. Door handles, body-color. Windshield, solar absorbing. Glass, solar absorbing. Wheels, 16\" (40.6 cm) aluminum. Wheel, spare, 16\" (40.6 cm) steel",
    email: "Hyundai@gmail.com",
    phone: "4014239893"
  },
  {
    postNumber: "3",
    Make: "Ford",
    Model: "Fusion Hybrid Titanium",
    year: "2018",
    ExtColor: "Gold",
    IntColor: "Gray",
    seattype: "Leather",
    Engine: "null",
    Drivetrain: "Front Wheel Drive",
    Trans: "1-Speed CVT w/OD",
    FuelType: "Hybrid",
    Mileage: "45401",
    MPG: "43/51",
    VIN: "3FA6P0RU7JR212106",
    zip: "63011",
    price: "16999",
    image1: "/assets/images/car3.jpg",
    image2: "https://media-cdn-delta.jazelc.com/media/5570804",
    image3: "https://media-cdn-delta.jazelc.com/media/5570805",
    image4: "https://media-cdn-delta.jazelc.com/media/5570809",
    description: "Bluetooth® Wireless, Heated Seats, SYNC, Camera, Keyless Entry, LED Headlamps, AM/FM/HD Radio, Push Button Start, EcoCruise Control, Remote Trunk Release, Fog Lights, Knee Air Bags, Alarm System",
    email: "Ford@gmail.com",
    phone: "2832928381"
  },
  {
    postNumber: "4",
    Make: "Toyota",
    Model: "Tacoma SR",
    year: "2017",
    ExtColor: "White",
    IntColor: "Gray",
    seattype: "cloth",
    Engine: "2.7L I-4",
    Drivetrain: "Rear Wheel Drive",
    Trans: "6-Speed Automatic w/OD",
    FuelType: "Gasoline",
    Mileage: "44240",
    MPG: "19/23",
    VIN: "5TFRX5GN0HX084437",
    zip: "63042",
    price: "16299",
    image1: "/assets/images/car4.jpg",
    image2: "https://media-cdn-delta.jazelc.com/media/5388368",
    image3: "https://media-cdn-delta.jazelc.com/media/5388360",
    image4: "https://media-cdn-delta.jazelc.com/media/5388364",
    description: "Auto Off Projector Beam Halogen Daytime Running Headlamps. Reverse Opening Rear Doors. Integrated Storage. Body-Colored Front Bumper w/1 Tow Hook. Regular Composite Box Style. Wheels: 16\" x 7J+30 Style Steel Disc. Wheels w/Silver. Accents w/Hub Covers. Body-Colored Manual Remote Side Mirrors w/Manual Folding. Fully Galvanized Steel Panels. Steel Spare Wheel",
    email: "Toyota@gmail.com",
    phone: "5112938356"
  }
]

}
