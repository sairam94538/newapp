import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeModuleModule } from './home-module/home-module.module';
import { SharedModuleModule } from './shared-module/shared-module.module';
import { UserControlModuleModule } from './user-control-module/user-control-module.module';





@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    SharedModuleModule,
    AppRoutingModule,
    HomeModuleModule,
    UserControlModuleModule
    

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
